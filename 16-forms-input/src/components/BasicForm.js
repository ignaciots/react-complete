import useFancyInput from "../hooks/use-fancy-input";

const validateName = (name) => name.trim() !== "";
const validateLastName = (name) => name.trim() !== "";
const validateEmail = (name) => name.trim().includes("@");

const BasicForm = (props) => {
  const {
    value: nameValue,
    hasErrors: nameHasErrors,
    isValid: isNameValid,
    onChangeHandler: nameChangeHandler,
    onBlurHandler: nameBlurHandler,
    reset: nameReset,
  } = useFancyInput(validateName);

  const {
    value: lastNameValue,
    hasErrors: lastNameHasErrors,
    isValid: isLastNameValid,
    onChangeHandler: lastNameChangeHandler,
    onBlurHandler: lastNameBlurHandler,
    reset: lastNameReset,
  } = useFancyInput(validateLastName);

  const {
    value: emailValue,
    hasErrors: emailHasErrors,
    isValid: isEmailValid,
    onChangeHandler: emailChangeHandler,
    onBlurHandler: emailBlurHandler,
    reset: emailReset,
  } = useFancyInput(validateEmail);

  let formIsValid = isNameValid && isLastNameValid && isEmailValid;

  const formSubmitHandler = (event) => {
    event.preventDefault();
    if (!formIsValid) {
      return;
    }

    console.log(nameValue);
    console.log(lastNameValue);
    console.log(emailValue);
    nameReset();
    lastNameReset();
    emailReset();
  };

  const firstNameClasses = nameHasErrors
    ? "form-control invalid"
    : "form-control";

  const lastNameClasses = lastNameHasErrors
    ? "form-control invalid"
    : "form-control";

  const emailClasses = emailHasErrors ? "form-control invalid" : "form-control";

  return (
    <form onSubmit={formSubmitHandler}>
      <div className="control-group">
        <div className={firstNameClasses}>
          <label htmlFor="name">First Name</label>
          <input
            type="text"
            id="name"
            onChange={nameChangeHandler}
            onBlur={nameBlurHandler}
            value={nameValue}
          />
          {nameHasErrors && <p>Name is invalid.</p>}
        </div>
        <div className={lastNameClasses}>
          <label htmlFor="name">Last Name</label>
          <input
            type="text"
            id="name"
            onChange={lastNameChangeHandler}
            onBlur={lastNameBlurHandler}
            value={lastNameValue}
          />
          {lastNameHasErrors && <p>Last name is invalid.</p>}
        </div>
      </div>
      <div className={emailClasses}>
        <label htmlFor="name">E-Mail Address</label>
        <input
          type="text"
          id="name"
          onChange={emailChangeHandler}
          onBlur={emailBlurHandler}
          value={emailValue}
        />
        {emailHasErrors && <p>E-Mail is invalid.</p>}
      </div>
      <div className="form-actions">
        <button disabled={!formIsValid}>Submit</button>
      </div>
    </form>
  );
};

export default BasicForm;
