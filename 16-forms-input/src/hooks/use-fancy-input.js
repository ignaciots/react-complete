import { useReducer } from "react";

const useFancyInput = (validate) => {
  const inputReducer = (state, action) => {
    switch (action.type) {
      case "BLUR":
        return {
          ...state,
          touched: action.payload,
        };
      case "VALUE":
        return {
          ...state,
          value: action.payload,
        };
      case "RESET":
        return {
          value: "",
          blur: false,
        };
      default:
        return state;
    }
  };

  const [inputState, dispatchStateUpdate] = useReducer(inputReducer, {
    value: "",
    touched: false,
  });

  const isValid = validate(inputState.value);
  const hasErrors = !isValid && inputState.touched;

  const onChangeHandler = (event) => {
    dispatchStateUpdate({
      type: "VALUE",
      payload: event.target.value,
    });
  };

  const onBlurHandler = (event) => {
    dispatchStateUpdate({
      type: "BLUR",
      payload: true,
    });
  };

  const reset = () => {
    dispatchStateUpdate({
      type: "RESET",
    });
  };

  return {
    value: inputState.value,
    isValid,
    hasErrors,
    onChangeHandler,
    onBlurHandler,
    reset,
  };
};

export default useFancyInput;
