import React, { useContext } from "react";
import TodoComponent from "./Todo";
import { TodosContext } from "../store/todos-context";
import classes from "./Todos.module.css";

const Todos: React.FC = (props) => {
  const ctx = useContext(TodosContext);
  return (
    <ul className={classes.todos}>
      {ctx.items.map((todo) => (
        <TodoComponent item={todo} onRemoveTodo={ctx.removeTodo} />
      ))}
    </ul>
  );
};

export default Todos;
