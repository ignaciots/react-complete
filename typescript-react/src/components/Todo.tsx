import React from "react";
import TodoType from "../models/todo";
import classes from "./Todo.module.css";

const Todo: React.FC<{ item: TodoType; onRemoveTodo: (id: string) => void }> = (
  props
) => {
  const { item, onRemoveTodo } = props;

  return (
    <li
      onClick={() => onRemoveTodo(item.id)}
      className={classes.item}
      key={item.id}
    >
      {item.text}
    </li>
  );
};

export default Todo;
