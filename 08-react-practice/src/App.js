import { Fragment } from "react";
import { useState } from "react";
import UserList from "./components/user/UserList";
import UserForm from "./components/user/UserForm";
import "./App.css";

const App = () => {
  const [users, setUsers] = useState([]);

  const createUserHandler = (user) => {
    setUsers((previousUsers) => [...previousUsers, user]);
  };

  return (
    <Fragment>
      <UserForm onCreateUser={createUserHandler} />
      <UserList users={users} />
    </Fragment>
  );
};

export default App;
