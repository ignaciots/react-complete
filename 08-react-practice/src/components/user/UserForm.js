import { useState, useRef } from "react";
import Button from "../ui/Button";
import ErrorModal from "../ui/ErrorModal";
import Card from "../ui/Card";
import Wrapper from "../helpers/Wrapper";

import "./UserForm.css";

const UserForm = (props) => {
  const nameInputRef = useRef();
  const ageInputRef = useRef();

  const [error, setError] = useState();

  const errorHandler = () => {
    setError(null);
  };

  const submitUserHandler = (event) => {
    event.preventDefault();
    const enteredName = nameInputRef.current.value;
    const enteredAge = ageInputRef.current.value;
    if (enteredName.trim().length === 0 || enteredAge.trim().length === 0) {
      setError({
        title: "Invalid input",
        message: "Please input a valid name and age (no empty values)",
      });
      return;
    }
    if (+enteredAge < 1) {
      setError({
        title: "Invalid age",
        message: "Please enter a valid age (> 0)",
      });
      return;
    }

    props.onCreateUser({
      id: Math.random(),
      name: enteredName,
      age: enteredAge,
    });
    nameInputRef.current.value = "";
    ageInputRef.current.value = "";
    setError();
  };

  return (
    <Wrapper>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <Card className="form">
        <form onSubmit={submitUserHandler}>
          <div>
            <div>
              <label htmlFor="username" className="form-label">
                Username
              </label>
              <input
                id="username"
                className="form-input"
                type="text"
                ref={nameInputRef}
              />
            </div>
            <div>
              <label htmlFor="age" className="form-label">
                Age (years)
              </label>
              <input
                id="age"
                className="form-input"
                type="number"
                ref={ageInputRef}
              />
            </div>
          </div>
          <div>
            <Button type="submit">Add User</Button>
          </div>
        </form>
      </Card>
    </Wrapper>
  );
};

export default UserForm;
