import User from "./User";
import Card from "../ui/Card";

import "./UserList.css";

const UserList = (props) => {
  if (props.users.length === 0) {
    return (
      <Card className="users">
        <p>There are no users.</p>
      </Card>
    );
  }
  return (
    <Card className="users">
      <ul>
        {props.users.map((user) => (
          <User key={user.id} user={user} />
        ))}
      </ul>
    </Card>
  );
};

export default UserList;
