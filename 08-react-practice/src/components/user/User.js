import "./User.css";

const User = (props) => {
  return (
    <li className="user">
      {`${props.user.name} (${props.user.age} years old)`}
    </li>
  );
};

export default User;
