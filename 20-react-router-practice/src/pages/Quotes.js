import QuoteList from "../components/quotes/QuoteList";

const DUMMY_QUOTES = [
  {
    id: "q1",
    author: "Nacho",
    text: "Quote #1",
  },
  {
    id: "q2",
    author: "Nacha",
    text: "Quote #2",
  },
];

const Quotes = () => {
  return <QuoteList quotes={DUMMY_QUOTES} />;
};

export default Quotes;
