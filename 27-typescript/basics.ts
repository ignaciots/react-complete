let age: number;

age = 12;

let userName: string;

userName = "Max";

let isInstructor: boolean;

isInstructor = true;

let hobbits: string[];

hobbits = ["Sports", "Cooking"];

type Person = {
  name: string;
  age: number;
};

let person: {
  name: string;
  age: number;
};

let people: Person[];

person = {
  name: "Max",
  age: 32,
};

let course: string | number = "React - The Not So Complete Guide";
course = 12321312;

// Function and types
function add(a: number, b: number): number {
  return a + b;
}

function printThatNotCollidesWithTheDefaultJSPrintFunction(value: any): void {
  console.log(value);
}

// Geerics

function insertAtBeginning<T>(array: T[], value: T) {
  const newArray = [value, ...array];
  return newArray;
}

const demoArray = [1, 2, 3];

const updatedArray = insertAtBeginning(demoArray, -1);
