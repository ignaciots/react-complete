import { createSlice } from "@reduxjs/toolkit";

const initialUiState = {
  cartShown: false,
  notification: null,
};

const toggleCartReducer = (state) => {
  state.cartShown = !state.cartShown;
  return state;
};

const uiSlice = createSlice({
  name: "ui",
  initialState: initialUiState,
  reducers: {
    showCart: toggleCartReducer,
    showNotification(state, action) {
      state.notification = {
        status: action.payload.status,
        title: action.payload.title,
        message: action.payload.message,
      };
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice.reducer;
