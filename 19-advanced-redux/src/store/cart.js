import { createSlice } from "@reduxjs/toolkit";

const initialCartState = {
  cartItems: [],
  changed: false,
};

const addItemReducer = (state, action) => {
  const existingCartItemIndex = state.cartItems.findIndex(
    (item) => item.id === action.payload.id
  );
  const existingCartItem = state.cartItems[existingCartItemIndex];
  let updatedItems = [...state.cartItems];
  if (existingCartItem) {
    const updatedItem = {
      ...existingCartItem,
      quantity: existingCartItem.quantity + 1,
      total: (existingCartItem.quantity + 1) * existingCartItem.price,
    };
    updatedItems[existingCartItemIndex] = updatedItem;
  } else {
    updatedItems = state.cartItems.concat({
      ...action.payload,
      quantity: 1,
      total: action.payload.price,
    });
  }

  return {
    cartItems: updatedItems,
    changed: true,
  };
};

const removeItemReducer = (state, action) => {
  console.debug("Deleting ", action);
  const existingCartItemIndex = state.cartItems.findIndex(
    (item) => item.id === action.payload.id
  );
  const existingCartItem = state.cartItems[existingCartItemIndex];

  let updatedItems;
  if (existingCartItem.quantity === 1) {
    updatedItems = state.cartItems.filter(
      (item) => item.id !== action.payload.id
    );
  } else {
    const updatedItem = {
      ...existingCartItem,
      quantity: existingCartItem.quantity - 1,
      total: (existingCartItem.quantity - 1) * existingCartItem.price,
    };
    updatedItems = [...state.cartItems];
    updatedItems[existingCartItemIndex] = updatedItem;
  }

  return {
    cartItems: updatedItems,
    changed: true,
  };
};

const replaceCartReducer = (state, action) => {
  state.cartItems = [...action.payload];
};

const cartSlice = createSlice({
  name: "cart",
  initialState: initialCartState,
  reducers: {
    add: addItemReducer,
    remove: removeItemReducer,
    replace: replaceCartReducer,
  },
});

export const cartActions = cartSlice.actions;
export default cartSlice.reducer;
