import classes from "./CartButton.module.css";
import { useSelector, useDispatch } from "react-redux";
import { uiActions } from "../../store/ui";

const CartButton = (props) => {
  const cartCountReducer = (acc, currentItem) => {
    return acc + currentItem.quantity;
  };

  const dispatch = useDispatch();
  const itemCount = useSelector((state) =>
    state.cart.cartItems.reduce(cartCountReducer, 0)
  );

  const cartToggleHandler = () => {
    dispatch(uiActions.showCart());
  };

  return (
    <button className={classes.button} onClick={cartToggleHandler}>
      <span>My Cart</span>
      <span className={classes.badge}>{itemCount}</span>
    </button>
  );
};

export default CartButton;
