#!/usr/bin/env sh
echo "Server started on port 1234."
 while true; do echo -e "HTTP/1.1 200 OK\nContent-Type: application/json\nAccess-Control-Allow-Origin: *\n\n$(cat src/assets/dummy_meals.json)" | nc -l -w 1 1234; done

echo "Server stopped."
