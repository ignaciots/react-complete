#!/usr/bin/env sh
echo "Meals server POST started on port 1235."
 while true; do echo -e "HTTP/1.1 200 OK\nContent-Type: application/json\nAccess-Control-Allow-Origin: *\n\n$(cat src/assets/dummy_meals.json)" | nc -l -w 1 1235; done

echo "Meals server POST stopped."
