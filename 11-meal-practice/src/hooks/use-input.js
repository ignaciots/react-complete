import { useReducer } from "react";

const inputDispatcher = (state, action) => {
  switch (action.type) {
    case "INPUT":
      return {
        ...state,
        value: action.payload,
      };
    case "TOUCH":
      return {
        ...state,
        touched: action.payload,
      };
    case "RESET":
      return {
        value: "",
        touched: false,
      };
    default:
      return state;
  }
};

const useInput = (validation) => {
  const [inputState, dispatchInput] = useReducer(inputDispatcher, {
    value: "",
    touched: false,
  });

  const isValid = validation(inputState.value);
  const hasErrors = !isValid && inputState.touched;

  const changeInputHandler = (event) => {
    dispatchInput({
      type: "INPUT",
      payload: event.target.value,
    });
  };

  const blurInputHandler = (event) => {
    dispatchInput({
      type: "TOUCH",
      payload: true,
    });
  };

  const resetInputHandler = (event) => {
    dispatchInput({
      type: "RESET",
    });
  };

  return {
    value: inputState.value,
    isValid,
    hasErrors,
    changeInputHandler,
    blurInputHandler,
    resetInputHandler,
  };
};

export default useInput;
