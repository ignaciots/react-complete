import useInput from "../../hooks/use-input";

import classes from "./CheckoutForm.module.css";

const fullNameValidator = (fullName) => {
  return fullName.trim() !== "";
};

const addressValidator = (address) => {
  return address.trim() !== "";
};

const CheckoutForm = (props) => {
  const {
    value: fullNameValue,
    isValid: fullNameIsValid,
    hasErrors: fullNameHasErrors,
    changeInputHandler: changeFullNameHandler,
    blurInputHandler: blurFullNameHandler,
    resetInputHandler: resetFullNameHandler,
  } = useInput(fullNameValidator);

  const {
    value: addressValue,
    isValid: addressIsValid,
    hasErrors: addressHasErrors,
    changeInputHandler: changeAddressHandler,
    blurInputHandler: blurAddressHandler,
    resetInputHandler: resetAddressHandler,
  } = useInput(addressValidator);

  const formIsValid = fullNameIsValid && addressIsValid;

  const formSubmitHandler = (event) => {
    event.preventDefault();
    if (!formIsValid) {
      return;
    }

    props.onSubmit(fullNameValue, addressValue);

    resetFullNameHandler();
    resetAddressHandler();
  };

  const fullNameClasses = fullNameHasErrors
    ? classes["input-error"]
    : classes["input"];
  const addressClasses = addressHasErrors
    ? classes["input-error"]
    : classes["input"];

  return (
    <form className={classes["checkout-form"]} onSubmit={formSubmitHandler}>
      <div>
        <label htmlFor="fullname">Full name</label>
        <input
          className={fullNameClasses}
          type="text"
          id="fullname"
          onChange={changeFullNameHandler}
          onBlur={blurFullNameHandler}
          value={fullNameValue}
        />
        {fullNameHasErrors && (
          <p className={classes["error-text"]}>Invalid full name.</p>
        )}
      </div>
      <div>
        <label htmlFor="address">Address</label>
        <input
          className={addressClasses}
          type="text"
          id="address"
          onChange={changeAddressHandler}
          onBlur={blurAddressHandler}
          value={addressValue}
        />
        {addressHasErrors && (
          <p className={classes["error-text"]}>Invalid address.</p>
        )}
      </div>
      <div className={classes["actions"]}>
        <button onClick={props.onClose} className={classes["button--alt"]}>
          Back
        </button>
        <button
          type="submit"
          disabled={!formIsValid}
          className={classes["button"]}
        >
          Submit
        </button>
      </div>
    </form>
  );
};

export default CheckoutForm;
