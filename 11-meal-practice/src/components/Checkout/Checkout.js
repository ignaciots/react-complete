import Modal from "../UI/Modal";
import { useContext, useCallback, useState, Fragment } from "react";
import CartContext from "../../store/cart-context";
import CheckoutItem from "./CheckoutItem";
import CheckoutForm from "./CheckoutForm";

import classes from "./Checkout.module.css";

const Checkout = (props) => {
  const cartCtx = useContext(CartContext);
  const totalAmount = `$${cartCtx.totalAmount.toFixed(2)}`;
  const items = cartCtx.items;
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [didSubmit, setDidSubmit] = useState(false);
  const onSubmittedHandler = props.onSubmitted;

  const postFormData = useCallback(async (formData) => {
    setIsSubmitting(true);
    await fetch("http://127.0.0.1:1235/", {
      method: "POST",
      body: JSON.stringify(formData),
    });
    setIsSubmitting(false);
    setDidSubmit(true);
  }, []);

  const checkoutItems = (
    <ul>
      {items.map((item) => (
        <CheckoutItem
          key={item.id}
          name={item.name}
          amount={item.amount}
          price={item.price}
        />
      ))}
    </ul>
  );

  const formSubmittedHandler = (fullname, address) => {
    console.log(fullname, address);
    console.table(items);
    postFormData({
      fullname,
      address,
      items,
    });
    onSubmittedHandler();
    cartCtx.clear();
  };

  const isSubmittingModalContent = <p>Sending order data...</p>;

  const cartModalContent = (
    <Fragment>
      <p className={classes["total-amount"]}>Total amount: {totalAmount}</p>
      <CheckoutForm onClose={props.onClose} onSubmit={formSubmittedHandler} />
    </Fragment>
  );

  const didSubmitModalContent = <p>Successfully sent the order!</p>;

  return (
    <Modal onClose={props.onClose}>
      {checkoutItems}
      {!isSubmitting && !didSubmit && cartModalContent}
      {isSubmitting && isSubmittingModalContent}
      {didSubmit && didSubmitModalContent}
    </Modal>
  );
};

export default Checkout;
