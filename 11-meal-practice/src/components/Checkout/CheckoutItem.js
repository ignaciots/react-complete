import classes from "./CheckoutItem.module.css";

const CheckoutItem = (props) => {
  const price = `$${props.price.toFixed(2)}`;

  return (
    <li className={classes["checkout-item"]}>
      <div className={classes["checkout-content"]}>
        <h2>{props.name}</h2>
        <div className={classes.summary}>
          <span className={classes.price}>{price}</span>
          <span className={classes.amount}>x {props.amount}</span>
        </div>
      </div>
    </li>
  );
};

export default CheckoutItem;
