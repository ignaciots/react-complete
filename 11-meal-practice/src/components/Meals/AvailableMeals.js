import { useEffect, useState, useCallback } from 'react';

import Card from '../UI/Card';
import MealItem from './MealItem/MealItem';
import classes from './AvailableMeals.module.css';

const AvailableMeals = () => {
  // TODO refactor into a reducer
  const [meals, setMeals] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [hasErrors, setHasErrors] = useState(false);

  const fetchAvailableMeals = useCallback(async() => {
    setIsLoading(true);
    setHasErrors(false);
    try {
      const response = await fetch("http://127.0.0.1:1234/",);
    if (!response.ok) {
      throw new Error(response.status);
    }

    const fetchedMeals = await response.json();
    setMeals(fetchedMeals);
    } catch(error) {
      setHasErrors(error.message);
    }
    setIsLoading(false);
  }, []);

  useEffect(() => {
    fetchAvailableMeals();
  }, [fetchAvailableMeals]);

  let mealsContainer;
  if (hasErrors) {
    mealsContainer = <p>An error occured while fetching meals: {hasErrors}</p>;
  } else if (isLoading) {
    mealsContainer = <p>Loading...</p>;
  } else {
      const mealsList = meals.map((meal) => (
    <MealItem
      key={meal.id}
      id={meal.id}
      name={meal.name}
      description={meal.description}
      price={meal.price}
    />
  ));
    mealsContainer = <ul>{mealsList}</ul>
  }



  return (
    <section className={classes.meals}>
      <Card>
        {mealsContainer}
      </Card>
    </section>
  );
};

export default AvailableMeals;
